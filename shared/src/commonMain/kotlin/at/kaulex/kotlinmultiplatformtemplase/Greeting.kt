package at.kaulex.kotlinmultiplatformtemplase

class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }
}