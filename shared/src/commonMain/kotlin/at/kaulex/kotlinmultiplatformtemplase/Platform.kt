package at.kaulex.kotlinmultiplatformtemplase

expect class Platform() {
    val platform: String
}