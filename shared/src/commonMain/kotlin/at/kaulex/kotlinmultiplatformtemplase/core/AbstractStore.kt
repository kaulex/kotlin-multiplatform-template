package at.kaulex.kotlinmultiplatformtemplase.core

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

interface State {
    var isLoading: Boolean
}

interface Action
interface Effect

// TODO have a look at https://proandroiddev.com/mvi-architecture-with-kotlin-flows-and-channels-d36820b2028d
//  (https://github.com/yusufceylan/MVI-Playground) and adapt?
abstract class Store<S : State, A : Action, E : Effect> : KoinComponent,
    CoroutineScope by CoroutineScope(Dispatchers.Main) {

    protected abstract val stateFlow: MutableStateFlow<S>
    protected abstract val sideEffectFlow: MutableSharedFlow<E>

    fun observeState(): StateFlow<S> = stateFlow
    fun observeSideEffect(): Flow<E> = sideEffectFlow

    @Throws(EffectException::class)
    protected abstract fun handleAction(action: A, oldState: S): S
    protected abstract fun errorEffect(throwable: Throwable): E

    fun dispatch(action: A) {

        val oldState = stateFlow.value

        val newState: S = try {
            this.handleAction(action, oldState)
        } catch (e: EffectException) {
            emitSideEffect(errorEffect(e))
            oldState
        }

        updateState(newState)
    }

    protected fun emitSideEffect(effect: E) {

        launch { sideEffectFlow.emit(effect) }
    }

    private fun updateState(newState: S) {
        if (newState != stateFlow.value) {
            stateFlow.value = newState
        }
    }

    protected fun S.loading() = apply { isLoading = true }
    protected fun S.finished() = apply { isLoading = false }
    protected fun S.fold(loading: () -> S, notLoading: () -> S): S {
        return if (isLoading) loading() else notLoading()
    }

}

abstract class EffectException(msg: String) : Exception(msg)
class LoadingException() : EffectException("Is loading")
class UnexpectedActionException : EffectException("Unexpected action")