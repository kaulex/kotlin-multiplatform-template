import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeTarget

val releaseBuild: String by project
version = "1.0"

plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    id("com.android.library")
}

kotlin {
    android()

    val iosTarget: (String, KotlinNativeTarget.() -> Unit) -> KotlinNativeTarget =
        if (System.getenv("SDK_NAME")?.startsWith("iphoneos") == true)
            ::iosArm64
        else
            ::iosX64

    iosTarget("ios") {
        binaries.framework {
            baseName = "Shared"

        }
    }

    sourceSets {
        named("commonMain") {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.0-native-mt") {
                    version {
                        strictly("1.6.0-native-mt")
                    }
                }
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2")

                // Ktor
                implementation("io.ktor:ktor-client-core:1.6.7")
                implementation("io.ktor:ktor-client-json:1.6.7")
                implementation("io.ktor:ktor-client-logging:1.6.7")
                implementation("io.ktor:ktor-client-auth:1.6.7")
                implementation("io.ktor:ktor-client-serialization:1.6.7")

                // Kodein
                implementation("org.kodein.db:kodein-db:0.9.0-beta")
                implementation("org.kodein.db:kodein-db-serializer-kotlinx:0.9.0-beta")

                implementation("io.insert-koin:koin-core:3.1.5")
                implementation("io.github.aakira:napier:2.4.0")
            }
        }

        named("commonTest") {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
                implementation("com.russhwolf:multiplatform-settings-test:0.8.1")
                implementation("io.insert-koin:koin-test:3.1.5")
            }
        }

        named("androidMain") {
            dependencies {
                implementation("io.ktor:ktor-client-android:1.6.7")
            }
        }

        named("androidTest") {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation("junit:junit:4.13.2")
            }
        }

        named("iosMain") {
            dependencies {
                implementation("io.ktor:ktor-client-ios:1.6.7")
            }
        }

        named("iosTest") {}
    }
}

android {
    compileSdk = 31
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdk = 21
        targetSdk = 31
    }
}