# Kotlin Multiplatform Template
Kotlin Multiplatform Template with xcodegen and some other fancy stuff.

## Get started Android
Everything stays the same as you know it from basic Android development. 

## Get started iOS 
Go to the ios directory and execute:
```
swift run xcodegen
```
This will create all necessary files for xcode.
You can open the project then with
```
open *.xc*
```