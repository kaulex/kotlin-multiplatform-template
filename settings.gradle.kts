pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "Kotlin_Multiplatform_Template"
include(":kmmTemplateAndroid")
include(":shared")