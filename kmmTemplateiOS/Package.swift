// swift-tools-version:5.5
import PackageDescription

// this package is only for command line dependencies, not used for dependencies used by any targets
let package = Package(
    name: "CMDLineDependencies",
    platforms: [
        .macOS(.v10_15),
    ],
    dependencies: [
        .package(url: "https://github.com/yonaskolb/XcodeGen.git", .upToNextMajor(from: "2.25.0"))
    ]
)
