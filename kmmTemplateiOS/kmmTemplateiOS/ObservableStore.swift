//
//  StoreObserver.swift
//  kmmTemplateiOS
//
//  Created by Fabian Schedler on 19.09.21.
//  Copyright © 2021 Fabian Schedler. All rights reserved.
//

import Foundation
import Shared

class ObservableStore<S : State, A : Action, E : Effect>: ObservableObject {
    @Published public var state: S
    @Published public var sideEffect: E?
    
    let store: Store<S, A, E>
    
    var stateWatcher : Closeable?
    var sideEffectWatcher : Closeable?
    
    init(store: Store<S, A, E>) {
        self.store = store
        self.state = store.observeState().value as! S
        
        stateWatcher = store.watchState().watch { [weak self] state in
            self?.state = (state as! S)
        }
        sideEffectWatcher = store.watchSideEffect().watch {[weak self] sideEffect in
            self?.sideEffect = (sideEffect as! E)
        }
    }
    
    public func dispatch(_ action: A) {
        store.dispatch(action: action)
    }
    
    deinit {
        stateWatcher?.close()
        sideEffectWatcher?.close()
    }
}
